FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["DssDemo1.csproj", "./"]
RUN dotnet restore "./DssDemo1.csproj"
COPY . .
RUN dotnet build "DssDemo1.csproj" -c Release -o /app
FROM build AS publish
RUN dotnet publish "DssDemo1.csproj" -c Release -o /app

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "DssDemo1.dll"]
